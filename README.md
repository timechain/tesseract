<a href="https://facebook.github.io/react/">
    <img src="https://assets.toptal.io/uploads/blog/category/logo/291/react.png" alt="react_logo" title="React.js" align="right" />
</a>

# **Have a question or suggestion?**
Contact us on [Linkedin](https://www.linkedin.com/company/timechaindotcom/), send an email to dev@timechain.com, or create a pull request on this project.

---

# Tesseract

Tesseract is a minimal web application using a few named techs, the app is [React](https://reactjs.org/) based and ready to start. Having a complete reactive cycle, it provides a functional example of how to continue the development.

The project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) to get its initial structure and configuration, where you can check initial [README](https://bitbucket.org/timechain/tesseract/src/develop/CRA_README.md) for more details and instructions.

---

## Index

- [Development Environment](#development-environment)
- [Stack](#stack)
- [Design Goals](#design-goals)
- [API](#api)
- [Folders and Files Structure](#folders-and-files-structure)
- [Avaialable Pages](#available-pages)
- [Getting Started](#getting-started)
- [Local Commands](#local-commands)
    - [Development](#development)
    - [Build](#build)
    - [Production](#production)
    - [JavaScript Lint](#javascript-lint)
    - [Flow Lint](#flow-lint)
    - [Test](#test)

---

##  Development Environment

This project is currently developed in a [Linux/GNU Ubuntu](https://www.ubuntu.com/) 16.04 LTS x86_64 OS, using [Visual Studio Code](https://www.visualstudio.com/) as IDE.


## Stack

* [Node.js](https://nodejs.org) (v10.5.0)
* [npm](https://www.npmjs.com) (6.1.0)
* [React](https://facebook.github.io/react) (16.4.1)
* [Redux](https://redux.js.org/) (4.0.0)
* [React Router DOM](https://reacttraining.com/react-router/) (4.3.1)
* [Flow](https://flow.org/) (0.77.0) for a static type checker for Javascript
* [Webpack](https://webpack.github.io) (3.8.1) for development and bundling
* [Babel](https://babeljs.io/) (6.26.0) transpiler for ES6 and ES7 magic
* [ESLint](http://eslint.org/) (4.10.0) to maintain a consistent code style
* SCSS as CSS extension language

---

## Design Goals

- Use Cutting-Edge Technologies
- Best Programming Techniques
- Babel 6 with Webpack and Hot Loader
- Fast testing with mocked-out DOM
- Separate [Smart and Dumb](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0) components
- Use [Flow](https://flow.org/) to obtain a safe Javascript environment
- Use only modern native CSS features to style the whole application. e.g. FlexBox, CSS Grid and etc.

---

## API

This application consumes data from the following services:

* [World Weather Online](https://developer.worldweatheronline.com/api/)

---

## Folders and Files Structure

---

## Available Pages

|    Page    |    URL        |                          Description                        |
|:----------:|:-------------:|:-----------------------------------------------------------:|
|    Home    |     / or /home        |  Main page of application to show some fetched information     |
|   NoMatch   |    * |  Any non-existent route/page     |

---

## Getting Started

Install yarn to a better and optmized package management

```sh
$ npm install --global yarn
```

Install application dependencies

```sh
$ yarn install
```

---

## Local Commands

In this current section you can find all commands to run the application in your machine. All the commands also are  in the `scripts` section of [package.json](package.json).

### Development

```sh
$ yarn start
```

Navigate to **http://localhost:3000/** to view the app.

### Build

```sh
$ yarn build
```

The above command is used to build the production files.

**Input:** `src/index.js` and `public/index.html`

**Output:** `build/`

### Production

```sh
$ yarn prod
```

The above command is used to build the production files and expose the files using a Nodejs server.

**Input:** `build/`

### JavaScript Lint

```sh
$ yarn lint
```

This above command is used to identifying and reporting on patterns in JavaScript on the specified directory.

### Flow Lint

```sh
$ yarn flow
```

This above command is used to identifying and reporting on patterns in Flow on the specified files.

### Test

```sh
$ yarn test
```

This above command is used to run all test in the entire project.

---

## Contributing

1. Fork it
2. Create your feature branch with specs (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

---

## Contributors

* Jaelson Carvalho ([jcarva](https://github.com/jcarva))

---

## Style Guide

Some parts of this project follow the style guide from [React Redux Universal Hot Example](https://github.com/erikras/react-redux-universal-hot-example)

---

## License

This project is licensed under the terms of the **GNU GENERAL PUBLIC** license.
>You can check out the full license [here](https://bitbucket.org/timechain/tesseract/src/develop/LICENSE)

---
