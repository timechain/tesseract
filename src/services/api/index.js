/* @flow */

// Services
import xhr from 'services/xhr'

// Configs
const apiBaseUrl = process.env.REACT_APP_API_BASE_URL ? process.env.REACT_APP_API_BASE_URL : 'process.env.REACT_APP_API_BASE_URL'
const apiKey = process.env.REACT_APP_API_KEY ? process.env.REACT_APP_API_KEY : 'process.env.REACT_APP_API_KEY'
const defaultParametrs = process.env.REACT_APP_API_DEFAULT_PARAMETERS ? process.env.REACT_APP_API_DEFAULT_PARAMETERS : 'REACT_APP_API_DEFAULT_PARAMETERS'

// End-Points
export const END_POINTS = {
  API_FETCH_CURRENT_WEATHER: `${apiBaseUrl}${apiKey}&q=`
}

// API Methods
class API {
  /**
   *
   * @param {string} city - city to get the current weather
   */
  fetchCurrentWeather = (city: string) => xhr.get(END_POINTS.API_FETCH_CURRENT_WEATHER + city + defaultParametrs)
}

export const api = new API()

export default api
