/* @flow */

/**
 * Function to handle, parse and adapt the response from Xhr service, before it reachs the reducer.
 * @param {Object} res - Success response from xhr service
 */
export const getCityWeatherPayloadAdapter = (city: string, res: Object) => {
  return {
    city,
    temperature: res.data && res.data.data.current_condition
      ? res.data.data.current_condition['0'].temp_C
      : 'The payload data structure was changed!'
  }
}

/**
 * Function to handle, parse and adapt the response from Xhr service, before it reachs the reducer.
 * @param {Object} res - Failure response from xhr service
 */
export const getCityWeatherErrorAdapter = (city: string, res: Object) => ({...res, city})
