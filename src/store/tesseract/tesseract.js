/* @flow */

// See https://github.com/erikras/ducks-modular-redux

// Services
import api from 'services/api/'

// Models
import type {Action as ActionType} from 'models/action'
import type {Tesseract as TesseractType} from 'models/tesseract'

// Adapters
import {getCityWeatherPayloadAdapter, getCityWeatherErrorAdapter} from './tesseract.adapter'

/**
 * Actions which manage the request from getCityWeather action creator.
 * @readonly @const {string}
 */
const GET_CITY_WEATHER_SUCCESS = 'tesseract/GET_CITY_WEATHER_SUCCESS'
const GET_CITY_WEATHER_FAILURE = 'tesseract/GET_CITY_WEATHER_FAIL'

/**
 * Initial Tesseract piece of state
 * @readonly @const {TesseractType}
 */
const initialState: TesseractType = {
  city: 'Montreal',
  temperature: 30
}

/**
 * Reducer which manage the current Tesseract state.
 * @param {State{}} TesseractType - The weather state.
 * @param {Action{}} ActionType - The triggered action.
 */
export default (state: TesseractType = initialState, action: ActionType) => {
  switch (action.type) {
    case GET_CITY_WEATHER_SUCCESS:
      console.log('Current weather:', action.payload)
      return action.payload

    case GET_CITY_WEATHER_FAILURE:
      console.log(action.error)
      return state

    default:
      return state
  }
}

// Action Creators
/**
 * Create the GET_CITY_WEATHER actions.
 * @param {string} city - The city to be shown.
 * @returns {object} The GET_CITY_WEATHER action which is a GET request for the city weather.
 */
export const getCityWeather = (city: string): Function => {
  const request = api.fetchCurrentWeather(city)
  return (dispatch: Function) => {
    request
      .then(res => dispatch({
        type: GET_CITY_WEATHER_SUCCESS,
        payload: getCityWeatherPayloadAdapter(city, res)
      }))
      .catch(res => dispatch({
        type: GET_CITY_WEATHER_FAILURE,
        error: getCityWeatherErrorAdapter(city, res)
      }))
  }
}
