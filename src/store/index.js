/* @flow */

// Dependencies
import { combineReducers } from 'redux'

// Reducers and Action Creators
import tesseract, {getCityWeather} from 'store/tesseract/tesseract'

export const actionCreators = {
  'GET_CITY_WEATHER': getCityWeather
}

export default combineReducers({tesseract})
