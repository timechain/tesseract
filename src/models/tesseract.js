/* @flow */

export type Tesseract = {
  city: string,
  temperature: number
}
