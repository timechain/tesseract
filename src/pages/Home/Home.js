/* @flow */

// Dependencies
import * as React from 'react'

// Components
import App from 'containers/App/App'

// Main Component
export const HomePage = () => <App />

export default HomePage
