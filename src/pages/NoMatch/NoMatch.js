/* @flow */

// Dependencies
import * as React from 'react'

// Main Component
export const NoMatchPage = () => <h1> No Match Page </h1>

export default NoMatchPage
