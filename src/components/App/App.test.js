// Dependecies
import * as React from 'react'
import ReactDOM from 'react-dom'

// Components
import AppUI from 'components/App/App'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<AppUI />, div)
  ReactDOM.unmountComponentAtNode(div)
})
