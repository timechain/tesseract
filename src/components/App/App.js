/* @flow */

// Dependencies
import * as React from 'react'

// Assets
import './App.css'

// Interfaces
import type {Tesseract as TesseractType} from 'models/tesseract'

export const AppUI = ({city, getCityWeather, logo, temperature}: {...TesseractType, getCityWeather: Function, logo: any}) => (
  <div className='App'>
    <header className='App-header'>
      <img src={logo} className='App-logo' alt='logo' />
      <h1 className='App-title'>Welcome to Tesseract</h1>
      <h1 className='App-title'>{city}, {temperature} °C</h1>
    </header>
    <p className='App-intro'>
      To get started, edit <code>src/containers/App/App.js</code> and save to reload.
    </p>
  </div>
)

export default AppUI
