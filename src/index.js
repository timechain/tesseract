// Dependencies
import * as React from 'react'
import ReactDOM from 'react-dom'

// Configs
import configureStore from 'store/config'
import registerServiceWorker from 'registerServiceWorker'

// Components
import Root from 'root/Root.component.js'

// Assets
import 'assets/styles/global.css'

ReactDOM.render(<Root store={configureStore()} />, document.getElementById('root'))
registerServiceWorker()
