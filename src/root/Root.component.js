/* @flow */

// Dependencies
import * as React from 'react'

// Components
import {Provider} from 'react-redux'
import {BrowserRouter as Router} from 'react-router-dom'
import Routes from './Routes.component.js'

// Interfaces
type Props = {
  store: Object
}

// Main Component
export const Root = ({store}: Props) => (
  <Provider {...{store}}>
    <Router>
      <Routes />
    </Router>
  </Provider>
)
export default Root
