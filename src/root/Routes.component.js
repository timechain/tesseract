/* @flow */

// Dependencies
import * as React from 'react'
import {Redirect, Route, Switch} from 'react-router-dom'

// Pages
import Home from 'pages/Home/Home'
import NoMatch from 'pages/NoMatch/NoMatch'

// Main Component
export default () => (
  <div id='routes'>
    {window.location.pathname.includes('index.html') && <Redirect to='/' />}
    <Switch>
      <Redirect exact from='/' to='/home' />
      <Route path='/home' component={Home} />
      <Route component={NoMatch} />
    </Switch>
  </div>
)
