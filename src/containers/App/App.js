/* @flow */

// Dependencies
import * as React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Components
import AppUI from '../../components/App/App'

// Assets
import logo from 'assets/images/logo.svg'

// Interfaces
import type {Tesseract as TesseractType} from 'models/tesseract'

// Action Creators
import {actionCreators} from 'store'
const getCityWeather = actionCreators.GET_CITY_WEATHER

// Main Component
export class App extends React.Component<{...TesseractType, getCityWeather: Function}> {
  componentWillMount = () => this.props.getCityWeather('João Pessoa')

  render () {
    return <AppUI {...{...this.props, logo}} />
  }
}

// Main component connection to the app's store
export default connect(
  (state: Object): Object => ({
    city: state.tesseract.city,
    temperature: state.tesseract.temperature
  }),
  (dispatch: Function): Object => bindActionCreators({ getCityWeather }, dispatch)
)(App)
